﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
	public bool release = false;
	public Ribbon[] ribbons;
	public Transform top;
	public Transform bottom;

	int numPoints = 30;
	public BoxCollider[] attachments = new BoxCollider[50];
	int n = 0;
	bool done = false;

	// Use this for initialization
	void Start () {
		float diff = top.position.y - bottom.position.y;
		float delta = diff / numPoints;
		attachments[n++] = CreateAttachment(bottom.position - new Vector3(0,0.1f,0));

		while (bottom.position.y + delta < top.position.y) {
			attachments[n++] = CreateAttachment(bottom.position + new Vector3(0, delta, 0));
			bottom.position += new Vector3(0, delta, 0);
		}
	}

	void Update() {
		if (!done && release) {
			StartCoroutine(ReleaseAttachments(0.01f));
			done = true;
		}
	}

	// Turning gameobject off will cause the cloth to naturally detach
	public IEnumerator ReleaseAttachments(float delay) {
		
//		for (int i = 0; i < (n+1)/2; i++) {
//			BoxCollider b = attachments[i];
//			b.gameObject.SetActive(false);
//			b = attachments[n-1-i];
//			b.gameObject.SetActive(false);
//			yield return new WaitForSeconds(delay);
//		}

		AudioSource a = GetComponent<AudioSource>();
		a.Play();
		yield return new WaitForSeconds (0.1f);

		for (int i = n-1; i >= 0; i--) {
			BoxCollider b = attachments[i];
			b.gameObject.SetActive(false);
//			yield return new WaitForSeconds(delay);
		}

	}

	BoxCollider CreateAttachment(Vector3 pos) {
		GameObject g = new GameObject ("Attachment");
		g.transform.position = pos;

		BoxCollider b = g.AddComponent<BoxCollider> ();
		g.AddComponent<Attachment> ();
		b.size = Vector3.one*0.5f + new Vector3(0, 0, 2);
		ribbons [0].transform.GetComponent<InteractiveCloth> ().AttachToCollider (b);
		ribbons [1].transform.GetComponent<InteractiveCloth> ().AttachToCollider (b);

		return b;

	}
}
