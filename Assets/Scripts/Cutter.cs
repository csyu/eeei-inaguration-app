﻿using UnityEngine;
using System.Collections;

public class Cutter : MonoBehaviour {

	Rigidbody r;
	public float speed = 25000;

	// Use this for initialization
	void Start () {
		r = GetComponent<Rigidbody> ();		
		r.AddForce(new Vector3(0, -1*speed, 0));
	
	}
	
}
