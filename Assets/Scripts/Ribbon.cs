﻿using UnityEngine;
using System.Collections;

public class Ribbon : MonoBehaviour {

	public BoxCollider[] colliders;
	public bool release = false;
	float delay = 0.3f;
	bool done = false;
	InteractiveCloth ic;

	void Start() {
		ic = GetComponent<InteractiveCloth> ();
	}

	// Update is called once per frame
	void Update () {
		if (release && !done) {
			StartCoroutine(CutRibbon());
			done = true;
		}
	}

	IEnumerator CutRibbon() {
		foreach (BoxCollider b in colliders) {
			ic.DetachFromCollider(b);
			yield return new WaitForSeconds(delay);
		}
	}
}
