using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchRecognizer : MonoBehaviour 
{
	
	public float minSwipeDistY;
		
	private Vector2 startPos;

	void Update()
	{
		//#if UNITY_ANDROID
		if (Input.touchCount > 0) 
		{
			
			Touch touch = Input.touches[0];
			
			
			switch (touch.phase) 
			{
				
			case TouchPhase.Began:
				startPos = touch.position;
				break;
			case TouchPhase.Ended:

				float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
				
				if (swipeDistVertical > minSwipeDistY) 
				{
					
					float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
					
					if (swipeValue < 0) {//down swipe
						Controller c = GetComponent<Controller>();
						StartCoroutine(c.ReleaseAttachments(0.01f));
					}
					else {
						Application.LoadLevel(Application.loadedLevel);
					}
							
				}
				break;
			}
		}
	}
}
